import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

public class frmMain extends JFrame {
	
	private static int x;
    private static int y;
    private boolean Moviendo;
    Timer tm;
    
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmMain frame = new frmMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmMain() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//JPanel
		contentPane = new JPanel();
		setBounds(0, 0, 1920, 1080);
		setUndecorated(true);
		setAlwaysOnTop(true);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setBackground(new Color(0.0f,0.0f,0.0f,0.0f));
		Image img = new ImageIcon(this.getClass().getResource("/Gato.gif")).getImage();
		
		contentPane.setLayout(null);
		
		
		//Label
		JLabel lblAsd = new JLabel("");
		lblAsd.setBounds(0, 0, 640, 480);
		contentPane.add(lblAsd);
		lblAsd.setIcon(new ImageIcon(img));
		
		lblAsd.addMouseListener(new MouseAdapter()  
		{  
		    public void mouseClicked(MouseEvent e)  
		    {  
		       //System.out.println("asd");
		       //lblAsd.setLocation(100, 100);
		    	
                //contentPane.setBounds(x, y, 640, 480);
		    	
		    }
		    
		    public void mousePressed(MouseEvent e)  
		    {  
		    	//x = e.getX();
                //y = e.getY();
		    	//Point point = MouseInfo.getPointerInfo().getLocation();
                //x = point.x;
                //y = point.y;
                //contentPane.setLocation(x, y);
		    	Point point = MouseInfo.getPointerInfo().getLocation();
		    	
		    	x = point.x;
                y = point.y;
		    	Moviendo = true;
		    	iniciartimer();
		    	
		    	
		    }
		    
		    public void mouseMoved(MouseEvent e) {
		    	
                //contentPane.setLocation(point);
                
		    }
		    
		    public void mouseReleased(MouseEvent e) {
		    	Moviendo = false;
		    }
		    
		    public void mouseDragged(MouseEvent e) {
                //Point point = MouseInfo.getPointerInfo().getLocation();
                //x = point.x - x;
                //y = point.y - y;
                //contentPane.setLocation(x, y);
		    	//lblAsd.setLocation(point.x - x, point.y - y);
                //lblAsd.setBounds(point.x - x,point.y - y,166,256);
		    	
		    }

		}); 
		
		//Timer
		tm = new Timer(10,new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (Moviendo == true) {
					Point point = MouseInfo.getPointerInfo().getLocation();
			    	
			    	x = point.x - 320;
	                y = point.y - 240;
					lblAsd.setBounds(x,y,640,480);
				}
			}
			
		});
		
	}
	
	public void iniciartimer() {
		tm.start();
	}
	
	
}
